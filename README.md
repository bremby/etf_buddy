# Dear investperson!

You've gained access to a tool for a madman, who thinks they can play the market by not putting everything in to one or two ETFs that cover the whole world; and instead scrambling over multiple ETFs, accounting for predictions of the future. 

Yes, the consensus among redditors at r/eupersonalfinance is to put cash regularly into either VWRL/VWCE or two ETFs - IWDA and EMIM - that cover developed and emerging markets individually. Basically, if you want to invest "safely" by historical data, then go ahead with VWRL/VWCE or IWDA+EMIM, and you're probably good.

If you're a renegade, a punk, a madman, an idiot, or all of the above (apes don't do ETFs), then you're going with multiple ETFs and you're asking the question: "Well hold on a minute? What if my ETFs overlap? I don't want to overinvest into just a few stonks!". Well, you're at the right bus stop. You need to download the ETF holdings yourself and put them in the right subdirectories with the right names, and then just run this script with "match" argument. It will read out the instruments / companies in those ETFs and see where else they repeat.

---

# FAQ

- "Where can I download the holdings data?"
- Depends:
	- iShares have it easy, just go to the page of the ETF (e.g. https://www.ishares.com/uk/individual/en/products/251767/?referrer=tickerSearch#/), go to Holdings, and at the bottom you have link "Detailed Holdings and Analytics". Download that.
	- Legal&General (L&G) has it similar, so e.g. https://fundcentres.lgim.com/uk/en/fund-centre/ETF/Hydrogen-Economy/?isin_code=IE00BMYDM794#Portfolio - this already scrolls you down to Portfolio. At the bottom you see "Download full fund holdings" with a link to a CSV.
	- Lyxor is the worst. You need to use the danish website, because the UK or the Luxembourg versions don't provide the data. So for example this one: https://www.lyxoretf.dk/en/instit/products/equity-etf/lyxor-msci-smart-cities-esg-filtered-dr-ucits-etf-acc/lu2023679256/gbp There you go to Holdings, switch to "Top 10 Fund Holdings", and the the right side a small icon of a document with .xls extension appears. Sneaky. Open that document in something (e.g. Libreoffice), cut out only the relevant data including the first line with field names, and save as .csv.
	- Han provides a .xls file, which you can directly export into a .csv file.
	- I tried looking up a list of holdings for a Vanguard ETF, but they provide only a scrollable list on a website, not a file. I don't feel like making a parser for that yet.
	- Invesco provides an Excel file: go to Portfolio, click View All at the Top Holdings part, then "Excel Download". Export into .csv as usual.

---

- "Where do I put the files?"
- Create a subdirectory named "ETFs" wherever you'll be running this script with $CWD (current working directory). The git repo already contains a bunch of examples for each supported fund manager. I have it at the same location. Place files into folders named "iShares", "LnG", or "Lyxor" for the ETF's you're interested in. I name the files with their ticker that I've bought it with, so e.g. "ETFs/iShares/IESG.csv". You can name it whatever, though, the script assumes that the filename before the first '.' is the name (ticker) of the ETF, but it has no effect. There are two ETFs as an example already. If you're gonna use longer names, the formatting will be off.

Do not put any other files there, as the script looks at everything. It will fail if it's not a well-formatted CSV file, or if it starts with a dot.

---

- "What is INRG2?"
- That's the newer iShares Global Clean Energy ETF after its expansion in April 2021.

---

- "What now?"
- Now run the script with "match" as an argument, i.e. "./etf_buddy.py match". It will print what it thinks.

---

- "I'm getting lots of warnings!"
- That's expected. You see, different fund managers report company names differently. So the script uses some heuristics to guess which company names are the same and which are too different. There are bound to be mistakes, but most of the time it should work. If you think you have way too many mismatches, tilting one way, you can play with the `passing_ratio` variable at the top of the script.

---

- "Why does the code look so horrible?"
- Because Python. I can't code in Python.

---

- "Why is there a 'match' argument at all?"
- Because there's also a 'download' argument. It used to work for iShares but not anymore, dunno why. Implementing it for other fund managers would be harder, because connecting there requires simulating a mouse click. So ignore this one.

---

- "I want to match against a different fund manager, like Vanguard or Amundi. How can I do that?"
- For now, get the data, edit it so that it looks like Lyxor data, and put it in the Lyxor folder. You don't need all fields, you only need 'Instrument Name' (company name). It relies on there being "Instrument Name" field containing company names.

---

- "I can't code. Can you please add parser for my fund manager?"
- Maybe. Ping me, u/bremby, on reddit. As mentioned before, I can't implement a Vanguard parser because I couldn't find an ETF holdings file.

---


# TODO
- I guess we could add optional arguments where you specify which subset of ETFs you wanna match instead of matching them all, so that you don't have to manually move files.
- Add functionality for when you're picking individual stocks.
- Add functionality for generic data / modified from other fund managers, or just specific parse functions to other managers.
