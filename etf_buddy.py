#!/usr/bin/env python3

import requests
import os
import csv
import difflib
# import json
import subprocess
import random
import sys
from urllib.parse import urlsplit,urlunsplit
from lxml import html

# you can edit this
passing_ratio = 0.88
warning_ratio_only = 0.83 



iShares_url = 'https://www.ishares.com'
iShares_search_url = '/uk/individual/en/search/summary-search-results?searchText={}&doTickerSearch=true'
                     # "/uk/individual/en/search/summary-search-results?searchText=inrg&doTickerSearch=true"

LnG_url = 'https://fundcentres.lgim.com'
LnG_search_url = '/uk/en/fund-centre/ETF/#SearchQuery={}'



# this used to work
def lookup_on_iShares(ticker):
	final_url = iShares_url + iShares_search_url.format(ticker)
	print(final_url)
	response = requests.get(final_url)
	print(response)
	# print(response.)
	if len(response.history) == 0: # if ticker exists, search is redirected and origin saved to history
		print("Ticker \"{}\" not found on iShares.".format(ticker))
		file = open(r"/tmp/etfbuddy-response-text", "w")
		file.write(response.text)
		file.close()
		return None
	# print(response.text)
	# tree = etree.parse(response.text, parser) # maybe this works?
	tree = html.fromstring(response.text)
	print(tree.xpath('/html/body/div[1]/div[2]/div/div/div/div/div/div[1]/div/div[2]/header[2]/div[5]/div[1]/h1')[0].text.strip())
	relative_file_url = tree.xpath('/html/body/div[1]/div[2]/div/div/div/div/div/div[11]/div/div/div/div[2]/a[1]')[0].get('href')
	holdings_url = iShares_url + relative_file_url

	# print(holdings_url)
	response = requests.get(holdings_url)
	# decoded_content = '\n'.join(response.content.decode('utf-8').split('\n')[iShares_csv_skip_lines:])
	decoded_content = response.content.decode('utf-8')
	actual_content = decoded_content[decoded_content.find('\xa0')+2:decoded_content.rfind('\xa0')]

	dr = csv.DictReader(actual_content.splitlines(), delimiter=',')
	filename = "ETFs/iShares/" + ticker
	file = open(filename, "w")
	for row in dr:
		# print(row.keys())
		# break
		if row['Exchange'] == 'CASH':
			continue
		file.write(row['Name'] + "\n")
	file.close()
	return True

def insert_company(company_name, etf_ticker, stocks_dict):
	inserted = False
	
	for oldcompany in list(stocks_dict):
		# simple direct compare. This simplifies stuff.
		if company_name == oldcompany:
			if etf_ticker not in stocks_dict[oldcompany]:
				stocks_dict[oldcompany] = stocks_dict[oldcompany] + [etf_ticker]
			inserted = True
			break
		# compare the first two words; if they match, it's most likely the same thing
		company_name_words = company_name.split()
		key_words = oldcompany.split()
		if len(company_name_words) > 1 and len(key_words) > 1:
			if company_name_words[0] == key_words[0] and company_name_words[1] == key_words[1]:
				if etf_ticker not in stocks_dict[oldcompany]: # technically the same company can appear mutliple times, when they have different types of shares
					stocks_dict[oldcompany] = stocks_dict[oldcompany] + [etf_ticker]
				inserted = True
				break
		# High similarity ratio, include it
		ratio = difflib.SequenceMatcher(None, company_name, oldcompany).ratio()
		if ratio >= passing_ratio and etf_ticker not in stocks_dict[oldcompany]:
			print("Warning: Assuming that \"{}\" is the same as \"{}\".".format(company_name, oldcompany))
			if etf_ticker not in stocks_dict[oldcompany]: # technically the same company can appear mutliple times, when they have different types of shares
				stocks_dict[oldcompany] = stocks_dict[oldcompany] + [etf_ticker]
			inserted = True
			break
		# quite high, just warn the user
		if ratio >= warning_ratio_only:
			print("Warning: doesn't seem like \"{}\" and \"{}\" are the same thing.".format(company_name, oldcompany))
	if not inserted:
		stocks_dict[company_name] = [etf_ticker]

def parse_ishares(ticker, filename, stocks_dict):
	file = open(filename, "r", newline='')
	file_contents = file.read()
	file.close()
	# cut away prefix and suffix, and split the string into list of lines, which is what DictReader expects
	actual_content = file_contents[file_contents.find('\xa0')+2:file_contents.rfind('\xa0')-1].splitlines()
	csv_dict = csv.DictReader(actual_content)
	i = 0
	for row in csv_dict:
		if row['Asset Class'] in ["Cash", "FX"]:
			continue
		insert_company(row['Name'], ticker, stocks_dict)
		i += 1
	return i
	
def parse_LnG(ticker, filename, stocks_dict):
	file = open(filename, "r", newline='', errors='ignore')
	file_contents = file.read()
	file.close()
	# cut away prefix and suffix, and split the string into list of lines, which is what DictReader expects
	actual_content = file_contents.splitlines()[16:-6]
	actual_content.pop(1) # the second line is always some crap
	# csv_dict = csv.DictReader(file_contents[file_contents.find('\xa0'):file_contents.rfind('\xa0')], delimiter=',')
	csv_dict = csv.DictReader(actual_content)
	# print(csv_dict.fieldnames)
	# print(csv_dict.__next__())
	i = 0
	for row in csv_dict:
		insert_company(row['COMPONENTS'], ticker, stocks_dict)
		i += 1
	return i

def parse_lyxor(ticker, filename, stocks_dict):
	file = open(filename, "r", newline='', errors='ignore')
	file_contents = file.read().splitlines()
	file.close()
	csv_dict = csv.DictReader(file_contents) # Lyxor provides data in xls, so I opened it in libreoffice and exported only the relevant data into a csv with default settings
	# print(csv_dict.fieldnames)
	i = 0
	for row in csv_dict:
		insert_company(row['Instrument Name'], ticker, stocks_dict)
		i += 1
	return i

def parse_Han(ticker, filename, stocks_dict):
	file = open(filename, "r", newline='', errors='ignore')
	file_contents = file.read()
	file.close()
	# cut away prefix and suffix, and split the string into list of lines, which is what DictReader expects
	actual_content = file_contents.splitlines()
	actual_content.pop(0) # the first line is the name of the ETF
	csv_dict = csv.DictReader(actual_content)
	i = 0
	for row in csv_dict:
		if row['Security Description'] in ["CASH"]:
			continue
		insert_company(row['Security Description'], ticker, stocks_dict)
		i += 1
	return i


def parse_Invesco(ticker, filename, stocks_dict):
	file = open(filename, "r", newline='', errors='ignore')
	file_contents = file.read()
	file.close()
	# cut away prefix and suffix, and split the string into list of lines, which is what DictReader expects
	actual_content = file_contents.splitlines()
	csv_dict = csv.DictReader(actual_content)
	i = 0
	for row in csv_dict:
		if row['Sector'] in ["Cash"]:
			continue
		insert_company(row['Name'], ticker, stocks_dict)
		i += 1
	return i

def match():
	stocks = dict()
	etfs = list()
	etf_sizes = list()
	etf_cooccurrences = dict()
	etf_cooccurrence_table = list(list())
	etf_cooccurrence_table_percentage = list(list())
	
	dirname = 'ETFs/iShares/'
	if os.path.exists(dirname):
		files = os.listdir(dirname)
		for f in files:
			etf_name = f[:f.find('.')]
			ret = parse_ishares(etf_name, dirname + f, stocks)
			etfs += [etf_name]
			etf_sizes += [ret]
	dirname = 'ETFs/LnG/'
	if os.path.exists(dirname):
		files = os.listdir(dirname)
		for f in files:
			etf_name = f[:f.find('.')]
			ret = parse_LnG(etf_name, dirname + f, stocks)
			etfs += [etf_name]
			etf_sizes += [ret]
	dirname = 'ETFs/Lyxor/'
	if os.path.exists(dirname):
		files = os.listdir(dirname)
		for f in files:
			etf_name = f[:f.find('.')]
			ret = parse_lyxor(etf_name, dirname + f, stocks)
			etfs += [etf_name]
			etf_sizes += [ret]
	dirname = 'ETFs/Han/'
	if os.path.exists(dirname):
		files = os.listdir(dirname)
		for f in files:
			etf_name = f[:f.find('.')]
			ret = parse_Han(etf_name, dirname + f, stocks)
			etfs += [etf_name]
			etf_sizes += [ret]
	dirname = 'ETFs/Invesco/'
	if os.path.exists(dirname):
		files = os.listdir(dirname)
		for f in files:
			etf_name = f[:f.find('.')]
			ret = parse_Invesco(etf_name, dirname + f, stocks)
			etfs += [etf_name]
			etf_sizes += [ret]


	# just initialize
	for etf in etfs:
		etf_cooccurrence_table += [[ 0 for _ in etfs]]
		etf_cooccurrence_table_percentage += [[ "" for _ in etfs]]

	for company in stocks:
		if len(stocks[company]) > 1:
			print("Company {} repeats in these ETFs: {}".format(company, stocks[company]))
			for etf in stocks[company]:
				etf_cooccurrences[etf] = etf_cooccurrences.get(etf, 0) + 1
				# magic
				for etf2 in stocks[company]:
					if etf == etf2:
						continue
					etf_cooccurrence_table[etfs.index(etf)][etfs.index(etf2)] += 1 

	for i in range(0,len(etfs)):
		for j in range(0,len(etfs)):
			etf_cooccurrence_table_percentage[i][j] = "{:.1f}%".format(100 * (etf_cooccurrence_table[i][j] / etf_sizes[i]))


	print("Final summary: number of ETF co-occurrences. If a company was listed mutliple times, a co-occurence occurred:")
	
	for etf in etf_cooccurrences:
		print("{:>5} : {:>4} ~ {:>5.2f}% (ETF size: {:3})".format(etf, str(etf_cooccurrences[etf]), 100*(etf_cooccurrences[etf]/etf_sizes[etfs.index(etf)]), etf_sizes[etfs.index(etf)]))

	print("Co-occurrences for each pair:")
	format_row = "{:>8}" * (len(etfs) + 1)
	print(format_row.format("", *etfs))
	for etf1, etf2s in zip(etfs, etf_cooccurrence_table):
		print(format_row.format(etf1, *etf2s))

	print("Percentage of the co-occurence between each pair, with the respect to the size of the ETF of the row:")
	format_row = "{:>8}" * (len(etfs) + 1)
	print(format_row.format("", *etfs))
	for etf1, etf2s in zip(etfs, etf_cooccurrence_table_percentage):
		print(format_row.format(etf1, *etf2s))

def main():
	if not os.path.exists("ETFs"):
		print("The \"ETFs\" subdirectory doesn't exist. Are you running me in the right directory?")
	# kill me now. Python doesn't have a switch statement. What is wrong with these people?!
	if sys.argv[1] == "download":
		# print("this doesn't work anymore for some reason. tough love, buddy.")
		# splits = line.replace("\n", "").partition(" ");
		# if len(splits) < 3 or len(splits[2].lstrip()) == 0:
		# 	print("missing ticker?")
		# ticker = splits[2].strip().upper()
		# ret = lookup_on_iShares(ticker)
		print("I'm sorry Dave, I'm afraid their websites are too complex for me to automagically download (without using a fake browser).")
	elif sys.argv[1] == "match":
		match()
	else:
		print("what did you just say about me?")

main()